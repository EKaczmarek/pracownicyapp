﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EmployeesList.Models
{
    public class Employee
    {
        ///<summary>
        /// Gets or sets EmployeeId.
        ///</summary>
        public int id { get; set; }

        ///<summary>
        /// Gets or sets Name.
        ///</summary>
        [DisplayName("imię")]
        [Required]
        [RegularExpression("^[a-zA-Z -ąćęłńóśźżĄĆĘŁŃÓŚŹŻ]*$", ErrorMessage = "Imię powinno składać się tylko z liter, spacji, myślnika")]
        public string name { get; set; }

        ///<summary>
        /// Gets or sets Country.
        ///</summary>
        [DisplayName("nazwisko")]
        [Required]
        [RegularExpression("^[a-zA-Z -ąćęłńóśźżĄĆĘŁŃÓŚŹŻ]*$", ErrorMessage = "Nazwisko powinno składać się tylko z liter, spacji, myślnika")]
        public string surname { get; set; }

        ///<summary>
        /// Gets or sets Birthdate.
        ///</summary>
        [DisplayName("data urodzenia")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        [DataType(DataType.DateTime)]
        [Required]
        public DateTime birthdate { get; set; }

        ///<summary>
        /// Gets or sets Age.
        ///</summary>
        [DisplayName("wiek")]
        public int age { get; set; }


        ///<summary>
        /// Gets or sets Position.
        ///</summary>
        [DisplayName("stanowisko")]
        [Required]
        [RegularExpression("^[a-zA-Z -#ąćęłńóśźżĄĆĘŁŃÓŚŹŻ]*$", ErrorMessage = "Stanowisko powinno składać się tylko z liter, spacji, myślnika, znaku #")]
        public string position { get; set; }

        ///<summary>
        /// Gets or sets Salary.
        ///</summary>
        [DisplayName("wynagrodzenie")]
        [Required]
        public int salary { get; set; }

        ///<summary>
        /// Gets or sets NIP.
        ///</summary>
        [DisplayName("NIP")]
        [MinLength(10)]
        [MaxLength(10)]
        [Required]
        [RegularExpression("^[0-9]*$", ErrorMessage = "NIP powinien zawierać tylko cyfry")]
        public string nip { get; set; }

        public Employee(int id, string name, string surname, DateTime birthdate, string position, int salary, string nip)
        {
            this.id = id;
            this.name = name;
            this.surname = surname;
            this.birthdate = birthdate;
            this.age = DateTime.Now.Year - birthdate.Year;
            this.position = position;
            this.salary = salary;
            this.nip = nip;
        }
        public Employee() { }
    }
}