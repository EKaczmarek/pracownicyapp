﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Xml.Linq;

namespace EmployeesList.Models
{
    public class EmployeesRepository
    {
        private List<Employee> employeesAll;
        private XDocument employeesData;
        
        public EmployeesRepository()
        {
                this.employeesAll = new List<Employee>();
                this.employeesData = XDocument.Load(System.Web.HttpContext.Current.Server.MapPath("~/XML/Database.xml"));
                var employee = from t in employeesData.Descendants("Employee")
                               select new Employee(
                                   (int)t.Element("id"),
                                   t.Element("name").Value,
                                   t.Element("surname").Value,
                                   ((DateTime)t.Element("birthdate")),
                                   t.Element("position").Value,
                                   (int)t.Element("salary"),
                                   t.Element("nip").Value);
                employeesAll.AddRange(employee.ToList<Employee>());      
        }
        public IEnumerable<Employee> getEmployees()
        {
            return this.employeesAll;
        }

        public Employee getEmployeeByID(int id)
        {
            return employeesAll.Find(item => item.id == id);
        }

        public void insertEmployee(Employee e)
        {
            e.id = (int)(from S in this.employeesData.Descendants("Employee")
                         orderby (int)S.Element("id")descending
                         select (int)S.Element("id")).FirstOrDefault() + 1;

            employeesData.Root.Add(new XElement("Employee", new XElement("id", e.id),
                new XElement("name", e.name),
                new XElement("surname", e.surname),
                new XElement("birthdate", e.birthdate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture)),
                new XElement("position", e.position),
                new XElement("salary", e.salary),
                new XElement("nip", e.nip)));

            employeesData.Save(HttpContext.Current.Server.MapPath("~/XML/Database.xml"));
        }

        public void deleteEmployee(int id)
        {
            employeesData.Root.Elements("Employee").Where(i => (int)i.Element("id") == id).Remove();
            employeesData.Save(HttpContext.Current.Server.MapPath("~/XML/Database.xml"));
        }

        public void editEmployee(Employee e)
        {
            XElement node = employeesData.Root.Elements("Employee").Where(i => (int)i.Element("id") == e.id).FirstOrDefault();
            node.SetElementValue("name", e.name);
            node.SetElementValue("surname", e.surname);
            node.SetElementValue("birthdate", e.birthdate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture));
            node.SetElementValue("position", e.position);
            node.SetElementValue("salary", e.salary);
            node.SetElementValue("nip", e.nip);

            employeesData.Save(HttpContext.Current.Server.MapPath("~/XML/Database.xml"));
        }
    }
}