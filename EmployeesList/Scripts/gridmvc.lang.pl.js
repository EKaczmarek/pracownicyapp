﻿/***
* Grid.Mvc pl language (pl-PL) http://gridmvc.codeplex.com/
*/
window.GridMvc = window.GridMvc || {};
window.GridMvc.lang = window.GridMvc.lang || {};
GridMvc.lang.pl = {
    filterTypeLabel: "Typ: ",
    filterValueLabel: "Wartość:",
    applyFilterButtonText: "Filtruj",
    filterSelectTypes: {
        Equals: "Równa się",
        StartsWith: "Rozpoczyna się",
        Contains: "Zawiera",
        EndsWith: "Kończy się",
        GreaterThan: "Większa od",
        LessThan: "Mniejsza od"
    },
    code: 'pl',
    boolTrueLabel: "Yes",
    boolFalseLabel: "No",
    clearFilterLabel: "Wyczyść"
};
