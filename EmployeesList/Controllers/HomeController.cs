﻿using EmployeesList.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using System.Xml.Linq;

namespace EmployeesList.Controllers
{
    public class HomeController : Controller
    {
        private EmployeesRepository employeesRepository;

        public HomeController() : this(new EmployeesRepository())
        {
        }
        public HomeController(EmployeesRepository repository)
        { 
            employeesRepository = repository;
        }

        public ViewResult Index()
        {
            return View(employeesRepository.getEmployees());
        }
  
        #region Adding
        public ActionResult Add()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Add(Employee e)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    employeesRepository.insertEmployee(e);
                    TempData.Add("SuccessMessage", string.Format("Dodano pracownika {0} {1}!", e.name, e.surname));
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    //error msg for failed insert in XML file
                    ModelState.AddModelError("", "Error creating record. " + ex.Message);
                    TempData.Add("FailMessage", string.Format("Nie dodano pracownika {0} {1}! ", e.name, e.surname));
                    return RedirectToAction("Index");
                }
            }
            return View();
        }
        #endregion
        #region Deleting
        public ActionResult Delete(int id)
        {
            Employee e = employeesRepository.getEmployeeByID(id);
            if (e == null)
                return RedirectToAction("Index");
            return View(e);
        }

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            string name = employeesRepository.getEmployeeByID(id).name;
            string surname = employeesRepository.getEmployeeByID(id).name;

            try
            {
                employeesRepository.deleteEmployee(id);

                TempData.Add("SuccessMessage", string.Format("Usunięto pracownika {0} {1}!", name, surname));
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                //error msg for failed delete in XML file
                TempData.Add("FailMessage", string.Format("Nie usunięto pracownika {0} {1}!", name, surname));
                return View("Index");
            }
        }
        #endregion
        #region Editting
        public ActionResult Edit(int id)
        {
            Employee e = employeesRepository.getEmployeeByID(id);
            if (e == null)
                return RedirectToAction("Index");
            return View(e);

        }
        [HttpPost]
        public ActionResult Edit(Employee e)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    employeesRepository.editEmployee(e);
                    TempData.Add("SuccessMessage", string.Format("Zmieniono dane pracownika {0} {1}!", e.name, e.surname));
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    //error msg for failed edit in XML file
                    TempData.Add("FailMessage", string.Format("Nie edytowano danych pracownika {0} {1}!", e.name, e.surname));
                    return View("Index");
                }
            }
            return View(e);
        }
        #endregion

    }
}